package me.cjd.sqlbuilder.test;

import me.cjd.sqlbuilder.core.SqlBuilder;
import me.cjd.sqlbuilder.engine.SqlBeetlEngine;
import me.cjd.sqlbuilder.engine.SqlFreemarkerEngine;
import me.cjd.sqlbuilder.model.SqlBuilderPara;

public class Tester {
	
	public static void main(String[] args) {
		// 设置 全局模板引擎
		SqlBuilder.setEngine(new SqlFreemarkerEngine());
		// Freemarker
		System.out.println("render:sql1");
		String sql1 = SqlBuilder.render("user.fromUser", new SqlBuilderPara("name", "颖"));
		System.out.println(sql1);

		System.out.println("render:sql2");
		String sql2 = SqlBuilder.sql("user.fromUserWhereId")
				.para("id",1231564)
				.para("name","张三")
				.render();
		System.out.println(sql2.trim());

		System.out.println("render:sql3");
		String sql3 = SqlBuilder.sql("user.fromUserWhereAge")
				.para("age",18)
				.render();
		System.out.println(sql3.trim());

	}
	
}