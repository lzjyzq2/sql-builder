不再为Java代码中难以维护的Sql语句而烦恼，SqlBuilder帮助您！
====

## 工具优势

整个工具大小仅36.6KB  
读取外部sql.md文件并缓存  
开放SqlRenderEngine接口让您可以自己渲染原生语句  
内部已集成只要引入jar包即可直接使用的模板引擎（Beetl、Freemarker）  

## 1分钟快速配置

### 代码中配置
```java
ConfigKit.me().setFolders(list);
ConfigKit.me().setSqlDebug(...);
ConfigKit.me().setSqlMid(...);
ConfigKit.me().setSqlMode(...);
```
### 使用配置文件
1. 将 sqlbuilder-config-default.properties 拷入您的项目src文件夹下  
2. 并将其更名为 sqlbuilder-config.properties  
3. sqlFolders 填入sql文件存放目录，多目录用逗号（,）分隔  
4. sqlMode 选择运行模式，run模式为产品模式读取缓存速度快，没有实时性；debug为开发模式，实时监测sql文件变化改动sql无需重启  
5. 引入 sql-builder-v1.3.5.jar 包  
6. Java 代码内调用    

```java

/**
 * 如果您需要使用模板引擎来渲染那么
 * 在这里设置一个全局引擎
 * 比如我使用beetl引擎  
 */
SqlBuilder.setEngine(new SqlBeetlEngine());  
  
// 获取sql语句，test为文件名称，findSqlOfBeetl为语句唯一id，重名则获取第一个  
String sql0 = SqlBuilder.render("test.findSqlOfBeetl");  

// 该传参构造在 v1.3.5 版本已不推荐使用
String sql1 = SqlBuilder.render("test.findSqlOfBeetl", new SqlBuilderPara("name", "颖"), ...); 

// 推荐使用
String sql2 = SqlBuilder.sql("test.findSqlOfBeetl")
// 不指定engine则使用全局引擎
// 指定模板渲染引擎，仅作用于该条语句，后设置会覆盖前设置
// 使用 freemarker 引擎
.engine(SqlFreemarkerEngine.class)
// 使用 beetl 引擎
.engine(SqlBeetlEngine.class)
// 设置参数
.para("name", "颖")
.para(..., ...)
// 渲染语句
.render();

```

## 测试用例
## user.md 文件
<pre>
- fromUser
这是注释  
```sql
select * from t_user where enable = 1 
```

- fromUserWhereId  
这也是注释  
```sql
select * from t_user where id = ${id?c?if_exists}
<#if name ??>
    and u_name = ${name}
</#if>
```

- fromUserWhereAge
这也是注释  
```sql
select * from t_user where age > ${age?c?if_exists}
```
</pre>

## Tester.java 测试类

```java
// 设置 全局模板引擎
SqlBuilder.setEngine(new SqlFreemarkerEngine());
// Freemarker
System.out.println("render:sql1");
String sql1 = SqlBuilder.render("user.fromUser", new SqlBuilderPara("name",颖"));
System.out.println(sql1);

System.out.println("render:sql2");
String sql2 = SqlBuilder.sql("user.fromUserWhereId")
		.para("id",1231564)
		.para("name","张三")
		.render();
System.out.println(sql2.trim());

System.out.println("render:sql3");
String sql3 = SqlBuilder.sql("user.fromUserWhereAge")
		.para("age",18)
		.render();
System.out.println(sql3.trim());

```

## 输出结果
```
render:sql1
select * from t_user where enable = 1 
render:sql2
select * from t_user where id = 1231564
    and u_name = 张三
render:sql3
select * from t_user where age > 18
```


## 更新日志
fork后  
[修改]修改支持的markdown风格

v1.3.5  
[新增] 增加新的构造方式 SqlBuilder.sql('fileName.sqlId').para(key, value).para(key, value).render();  

v1.3.4  
[修复] SqlBuilderPara 传 null 值会发生错误的问题  

v1.3.3  
[修复] SqlBuilderModel 无法toJSONString的问题  

v1.3.2  
[新增] SqlBuilderModel 类支持jfinal的快速使用  
[新增] 配置文件 sqlDebug=true 调试模式，输出渲染前后语句以及参数  

v1.3.1  
[优化] 达梦数据库SQL语句渲染代码  

V1.3  
[新增] sql('...') 语句引入函数支持  
[新增] 中间件接口，提升扩展性  

V1.2  
[新增] log4j 日志  
[新增] 国产达梦数据库sql语句渲染支持  
[优化] 使用Sonarqube对代码质量进行优化  
[修复] 修复了一些小bug  
[测试] 根据在正式商业项目中上线使用进行的优化调整  
  
v1.1  
[优化] 移除了对 apache-commons-lange3工具包的依赖  
  
v1.0  
[出生] 项目首次登录 git.  

------
原作者：cjd，
本项目：fork于 https://gitee.com/unknow0409/sql-builder
