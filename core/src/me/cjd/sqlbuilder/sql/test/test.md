- findTestForStatus
```sql
select * from T_TEST AS i where i.status = ? 
```

- findSqlOfFreemarker
```sql
select * from T_TEST AS i <#if order ??>ORDER BY i.date ${order} </#if>
```

- findSqlOfBeetl
```sql
select * from T_TEST AS i <%if (!isEmpty(name)) {%>where i.name = '${name}' <%}%>
```

- findSqlOfVelocity
```sql
select * from T_TEST AS i #if(order != null) ORDER BY i.id ${order} #end
```
- orderUser
```sql
order by ${orderName!} ${orderRule!}
```